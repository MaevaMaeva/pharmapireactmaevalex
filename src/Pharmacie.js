import React from 'react';
//--------penser à faire un 'npm install axios --save'---------
//import d'axios pour pouvoir faire la requete à l'API
import axios from 'axios';

// le component Pharmacie affiche le détail de chaque pharmacie qui sera présente dans la liste affichée par Pharmalist
class Pharmacie extends React.Component {
    //le constructor permet de mettre  en place le state(pour pouvoir lui affecter un objet)
    //dans le state, on met une clé valeur ********************************
    constructor(props){
        // Ne pas appeler `this.setState()` ici !
        super(props);
            this.state={
                
            }
            this.updatePharma= this.updatePharma.bind(this);
        
    }
    // Fonction qui va nous permettre de supprimer une pharmacie
    // elle est reliée au bouton Supprimer dans lequel on a fait passer 
    // this.props.fiche.id qui correspond à l'id de la pharmacie affichée par le component Pharmacie
    // cet id est ici passé en paramètre de deletePharma pour nous permettre, pendant l'appel à l'API
    // de préciser dans l'url, l'id de la pharmacie ciblée pour pouvoir la supprimer
    deletePharma(id){
        console.log("salut, je suis deletePharma, je veux supprimer la pharmacie qui a l'id : "+id)
        // Requête Axios qui permet de supprimer une pharmacie sélectionné grâce à la méthode .delete
        axios.delete('http://127.0.0.1:8000/pharma/'+id)
        // la suppression se fait uniquement avec l'appel de l'url et la methode delete, 
        // le .then n'est pas spécialement utile et le .catch attrape les erreurs comme d'habitude
        .then(() => alert('La pharmacie '+id+' a été supprimée avec succès'))
        .catch(e => console.log(e));

    }
    //Fonction qui va mettre à jour une pharmacie existante dans l'API
    
    updatePharma(fiche) {
        console.log("salut, je suis updatePharma, je veux mettre à jour la pharmacie "+fiche.nom)
        this.props.onModificationChange(fiche);
      }
    render() {
        
       
        return (
           // Dans Pharmalist.js, nous faisons passer l'objet-pharmacie dans le props 'fiche' du component Pharmacie
           // ce qui signifie qu'ici, this.props.fiche correspond à un objet-pharmacie donné.
           // notre but est donc d'afficher les détails de cette pharmacie dans un élément de liste
            <li key={this.props.fiche.id}>
                
                    <h3>{this.props.fiche.nom}</h3>
                    
                    <p>{this.props.fiche.quartier}</p>
                    <p>{this.props.fiche.ville}</p>
                
                <p>Jour de garde : {this.props.fiche.garde}</p>
                
                <button onClick={() => this.deletePharma(this.props.fiche.id)}>Supprimer</button>
                <button onClick={() => this.updatePharma(this.props.fiche)}>Modifier</button>
                
            </li >
            
            
          
            );
        
    
    }
}

//on exporte pour pouvoir se servir du component Pharmacie ailleurs
//--> notamment dans PharmaListe.js
export default Pharmacie;

//POINT SUR LA METHODE RENDER dans le fichier PharmaListe.js