import './App.css';

function Garde(props){
    // Dans App.js, nous faisons passer le props 'onChange' du component Garde
    
    return (
                
        <label>Trier par :
        <select name="day" onChange={props.onChange}>
            <option value="*">Toutes les pharmacies</option>
            <option value="aujourd'hui">De garde Aujourd'hui</option>
            <option value="lundi">De garde le lundi</option>
            <option value="mardi">De garde le mardi</option>
            <option value="mercredi">De garde le mercredi</option>
            <option value="jeudi">De garde le jeudi</option>
            <option value="vendredi">De garde le vendredi</option>
            <option value="samedi">De garde le samedi</option>
            <option value="dimanche">De garde le dimanche</option>
        </select>
        </label>
        
    );
}
//on exporte pour pouvoir se servir du component Garde ailleurs
//--> notamment dans App.js
export default Garde;