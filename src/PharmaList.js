import React, { useState, useEffect } from 'react';

//on va se servir du component Pharmacie pour afficher 
//les listes de pharmacies une fois qu'on aura fait notre appel à l'api
import Pharmacie from './Pharmacie';

//--------penser à faire un 'npm install axios --save'---------
//import d'axios pour pouvoir faire la requete à l'API
import axios from 'axios';

import FormulaireAuxPetitsOignons from './FormulaireAuxPetitsOignons';


// Le component Pharmalist va nous servir à afficher la liste des pharmacies récupérées dans l'API
class PharmaList extends React.Component {

    //le constructor permet de mettre  en place le state(pour pouvoir lui affecter un objet)
    //dans le state, on met une clé valeur 'pharmacies' qui sera sous forme de tableau pour 
    //stocker nos listes d'objets-pharmacies par la suite
    constructor(props) {
        // Ne pas appeler `this.setState()` ici !
        super(props);
        this.state = { 
            pharmacies: [],
            modif:false,
            aModifier:{}
            
        };
        //Le constructeur est le seul endroit où on peut affecter directement une valeur à this.state. 
        //Dans toutes les autres méthodes, il faut utiliser `this.setState()`.

        this.handleModifChange = this.handleModifChange.bind(this);
    }

    //componentDiMount() est une fonction qui ne va s'exécuter qu'une fois à l'appel du component PharmaList 
    
    componentDidMount() {
        //la requête axios va chercher les infos dans l'api grâce au .get, 
        //le .then lui, stocke le résultat de la requête dans le state du component PharmaList
        // grâce à la méthode setState() qui insère dans la clé valeur 'pharmacies' 
        //du this.state, les données récupérées dans l'API
        axios.get('http://127.0.0.1:8000/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                this.setState({ pharmacies: pharmapi.data });
            });
    }
    handleModifChange(fiche){
        this.setState({modif:true, aModifier: fiche})
    }
    render() {
        // this.state.pharmacies correspondant à une liste d'objets, qu'on va filtrer grâce à .filter() 
        //de manière à ne 'laisser dans le tableau' uniquement les pharmacies qui ont le jour de garde correspondant
        // à la props 'garde' (ici 'this.props.filter') passée dans le 'filter' du component PharmaList (ceci est effectué dans App.js).

        //Si, aucune props 'garde' n'a été modifiée, alors elle correspond à '*' et on voudra tout afficher
        //.map va nous permettre de boucler sur le tableau préalablement filtré, de manière à faire
        //passer chaque pharmacie en tant que props (qu'on appelle 'fiche') dans le component Pharmacie 
        //pour pouvoir afficher les listes (cf Pharmacie.js).
        

        //méthode filter()>> https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
        //méthode map()>> https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/map
        return (
            <>
             {this.state.modif === false &&
            <ul>
                
                {this.state.pharmacies
                    .filter(pharma =>  this.props.tri === pharma.garde 
                        || this.props.tri === '*'
                    )
                    .map(pharmacie => (
                        <Pharmacie key={pharmacie.id} fiche={pharmacie} onModificationChange={this.handleModifChange}/>
                    ))
                }
                 
            </ul>
       }
       {this.state.modif === true &&
       <>
       <h3>Modifier La pharmacie : {this.state.aModifier.nom} </h3>
            <FormulaireAuxPetitsOignons fiche={this.state.aModifier}/>
            </>
       }
       </>
        )
    }
}
//Pharmacie et FormulaireAuxPetitsOignons sont ici les components enfants du component PharmaList

//on exporte pour pouvoir se servir du component PharmaList ailleurs
//--> notamment dans App.js
export default PharmaList;



/*LE MODE FONCTION (PLUTOT QUE LE MODE CLASSE) RESSEMBLERAIT A CA :
function PharmaList() {

    // Les Hooks sont une nouveauté de React 16.8. Ils permettent de bénéficier 
    // d’un état local et d’autres fonctionnalités de React sans avoir à écrire de classes.
    // ---> https://fr.reactjs.org/docs/hooks-state.html

    // donc au lieu d'utiliser un constructor et un state, nous allons utiliser la syntaxe suivante :
    // const [variableDEtat, setVariableDEtat] = useState('valeurQuOnAttribueAVariableDEtat')
    // useState() nécessite qu'on l'importe en début de fichier
    //(import React, { useState, useEffect } from 'react';)
    // setVariableDEtat est l'équivalent de this.setState
    const [pharmacies, setPharmacies] = useState([]);

    // useEffect() est l'équivalent de componentDidMount() qui ne va s'exécuter qu'une fois à l'appel du component PharmaList
    //la requête axios va chercher l'api grâce au .get, 
    //le .then lui, stocke le résultat de la requête dans la variable d'état du component PharmaList
    // grâce à la méthode setPharmacies() qui insère dans variable d'état 'pharmacies' les données récupérées dans l'api (soit 'pharmapi.data')
    
    useEffect(() => {
        axios.get('http://127.0.0.1:8080/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                setPharmacies(pharmapi.data);
            });
    }, []);

    // pharmacies correspondant à une liste d'objets-pharmacies, 
    //.map va nous permettre de boucler sur le tableau, de manière à faire
    //passer les ids de chaque pharmacie et chaque pharmacie elle-même en tant que props dans le component Pharmacie 
    //pour pouvoir afficher les listes (cf Pharmacie.js). 

    
    //méthode map()>> https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/map
    // Pas besoin de render() dans une fonction.
    return (
        <article>
            <h2>Liste des pharmacies</h2>
            <ul>
                {pharmacies
                    .map(pharmacie => (
                    <li key={pharmacie.id}>
                        {pharmacie.nom}
                    </li>
                ))}
            </ul>
        </article>
    )
}
*/

/* POINT SUR LA METHODE RENDER
La méthode render() est la seule méthode requise dans une classe de composant.

Lorsqu’elle est appelée, elle examine en général this.props et this.state et 
renvoie un des types suivants :

    Éléments React. Typiquement créés via JSX. Par exemple, <div /> et <MyComponent /> sont des éléments React qui demandent à React de produire, respectivement, un nœud DOM et un autre composant défini par l’utilisateur.
    Tableaux et fragments. Ils vous permettent de renvoyer plusieurs éléments racines depuis un rendu. Consultez la documentation des fragments pour plus de détails.
    Portails. Ils permettent d’effectuer le rendu des enfants dans une autre partie du DOM. Consultez la documentation des portails pour plus de détails.
    Chaînes de caractères et nombres. Ils deviennent des nœuds textuels dans le DOM.
    Booléens ou null. Ils ne produisent rien. (Ça existe principalement pour permettre des motifs de code tels que return test && <Child />, ou test serait booléen.)

La fonction render() doit être pure, 
c’est-à-dire qu’elle ne doit rien changer à l’état local du composant, 
doit renvoyer le même résultat chaque fois qu’elle est invoquée (dans des conditions identiques), 
et ne doit pas interagir directement avec le navigateur.

Si vous avez besoin de telles interactions, 
faites-le plutôt dans componentDidMount() ou d’autres méthodes de cycle de vie. 
S’assurer que render() reste pure facilite la compréhension du fonctionnement des composants.*/