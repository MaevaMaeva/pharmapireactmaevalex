import React from 'react';
import axios from 'axios';
import './App.css';



/* Le formulaire est appelé par le component App pour créer une nouvelle pharmacie
Il est également appelé par Pharmalist pour en modifier une existante
Dans le second cas, on lui fait passer une props 'fiche' qui correspond à l'objet-pharmacie
à modifie */
class FormulaireAuxPetitsOignons extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            nom:'',
            quartier:'',
            ville:'',
            garde: '',
            id:'',
        };
       
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUp = this.handleUp.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    componentDidMount() {
        
        //Avant toute chose, grâce à componentDidMount, on va exécuter UNE fois ce code :
        // Si il y a quelque chose dans les props 
        //(donc, si le formulaire est appelé par Pharmalist dans le but de modifier une pharmacie existante)
        // on va donc tester si on a un props fiche et si c'est le cas, changer le state de FormulaireAuxPetitsOignons
        
            
            if (this.props.fiche){
            
                this.setState({
                    nom : this.props.fiche.nom, quartier : 
                    this.props.fiche.quartier, 
                    ville : this.props.fiche.ville, 
                    garde : this.props.fiche.garde, 
                    id : this.props.fiche.id
                })
          
            console.log("y'a un truc dans props"+this.props.fiche.nom)}
        

                //on en profite pour conserver l'id dans cette page 
                //car il va nous servir à faire la requete axios sur une pharmacie donnée
    
        }
    handleUp(event){
        // event.preventDefault ==> "Ne fais pas ce que tu fais habituellement ! J'ai d'autres projets pour toi !"
        //event.preventDefault va nous servir notamment à empecher de rafraichir la page,
        // ou changer de page... evenements qui habituellement sont prévus quand on a un submit
        // là ça va nous gêner donc on lui demande de pas le faire.
        event.preventDefault();
        
        console.log('on est dans le UP'+event)
        alert('Vous avez enregistré la pharmacie : ' + this.state.nom + ' du quartier : '+ this.state.quartier + ' à : '+this.state.ville+' de garde le : '+ this.state.garde);
        
        axios.put(
            'http://127.0.0.1:8000/pharma/'+this.state.id,
              {nom : this.state.nom,
              quartier: this.state.quartier,
              ville: this.state.ville,
              garde:this.state.garde
              }
        )
        .then(r => console.log(r.status))
        .catch(e => console.log(e));
        //vider le state :
        this.setState({
          nom: '',
          quartier: '',
          ville: '',
          garde: '',
          id:''
      });
        
    }
    handleInputChange(event) {
        console.log("handleInputChange à l'appareil, j'écoute "+ event.target.value)
        console.log("HELP, que veut dire event.target.name ? "+ event.target.name)
    
        this.setState({
            [event.target.name] :event.target.value
        });
        console.log("est ce qu'on a imprimé un truc : "+this.state.nom+this.state.quartier+this.state.ville+this.state.garde)
      }
   
    handleSubmit(event) {
        console.log("handleSubmit à l'appareil, j'écoute rien"+event)
        alert('Vous avez enregistré la pharmacie : ' + this.state.nom + ' du quartier : '+ this.state.quartier + ' à : '+this.state.ville+' de garde le : '+ this.state.garde);
        event.preventDefault();

        axios.post(
          'http://127.0.0.1:8000/pharma',
            {nom : this.state.nom,
            quartier: this.state.quartier,
            ville: this.state.ville,
            garde:this.state.garde
            }
      )
      .then(r => console.log(r.status))
      .catch(e => console.log(e));
      //vider le state :
      this.setState({
        nom: '',
        quartier: '',
        ville: '',
        garde: ''
    });
    }
    
    render() {
            
        return (
            
            <form onSubmit={this.props.fiche ? this.handleUp: this.handleSubmit}>
                <div>
                    <label>
                    Nom : 
                    <input type="text" name='nom' attend={this.state.nom} value={this.state.nom} onChange={this.handleInputChange}  />        
                    </label>
                </div>
                <div>
                <label>
                    Quartier :
                    <input type="text" name='quartier' value={this.state.quartier} onChange={this.handleInputChange} />        
                    </label>
                </div>
                <div>
                    <label>
                        Ville :
                        <input type="text" name='ville' value={this.state.ville} onChange={this.handleInputChange} />        
                    </label>
                </div>
                <div>
                <label>
                    Jour de garde :
                    <select name='garde' value={this.state.garde} onChange={this.handleInputChange}>
                        <option value="*">Choisir</option>
                        <option value="lundi">Lundi</option>
                        <option value="mardi">Mardi</option>
                        <option value="mercredi">Mercredi</option>
                        <option value="jeudi">Jeudi</option>
                        <option value="vendredi">Vendredi</option>
                        <option value="samedi">Samedi</option>
                        <option value="dimanche">Dimanche</option>
                    </select>
                    </label>
                </div>
                <button type="submit" value="Envoyer" >Enregistrer</button>
            </form>
        
        );
    }
  }

//on exporte pour pouvoir se servir du component FormulaireAuxPetitsOignons ailleurs
//--> notamment dans App.js et Pharmalist.js
export default FormulaireAuxPetitsOignons;