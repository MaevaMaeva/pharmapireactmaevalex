import React, { useState } from 'react';
import Menu from './Menu';
import PharmaList from './PharmaList';
import Garde from './Garde';
import FormulaireAuxPetitsOignons from './FormulaireAuxPetitsOignons';
import './App.css';

function App() {
  //useState est une méthode qui 
  const [page, setPage] = useState('page1');
  const [garde, setGarde] = useState('*');

  const menuClick1 = id => {window.location.reload();setPage(id)};
  const menuClick = id => {setPage(id)};
  const gardeClick = day => setGarde(day.target.value);

  return (
    <div className="App">
      <header className="App-header">
        <span>Consultez et modifiez les pharmacies autour de chez vous, parce que c'est vous le patron !</span>
        <nav>
          <Menu title="Liste des pharmacies" id="page1" onClick={menuClick1} />
          <Menu title="Gérer les pharmacies" id="page2" onClick={menuClick} />
        </nav>
      </header>
      <main>

        {/*la syntaxe qui suit est EQUIVALENTE à un if:
        if (page==="page1"){ faire ce qu'il y a entre <> et </> }
        ......un peut comme si '&&' voulait dire 'alors' */}

        {page === "page1" &&
          // Les chevrons <> et </> sont des encapsuleurs un peu comme des divs 
          // et vont nous servir à contourner le fait qu'on a le droit de mettre 
          // qu'un seul élément après un '&&', comme après un 'return', d'ailleurs.
          <>
               <Garde onChange={gardeClick} className="triage" />
          {/* si garde est différent de toutes les pharmacies (comme par exemple = à mardi) 
          alors affiche en titre les jours de garde */}
            {garde !== '*' &&
              <h2>
                Les pharmacies de garde le
                <span className="App-link"> {garde}</span>
              </h2>
            }
          {/* si garde est égal à toutes les pharmacies, alors on affiche  le titre 'toutes les pharmacies' */}
            {garde === '*' &&
              <h2>
                <span className="App-link">Toutes </span>
                les pharmacies
              </h2>
            }
            {/* Dans tous les cas, si on est sur la page1, on affiche le component Pharmacie */}
            <PharmaList tri={garde} />
          </>
        }
        {/* En revanche si on est sur la page2, on affiche la possibilité de modifier ajouter supprimer */}
        {page === "page2" &&
          <>
          <h2>Nouvelle pharmacie</h2>
          <FormulaireAuxPetitsOignons/>
          </>
        }

      </main>
      <footer className="App-footer">
        <h4>© Copyright MivAlex</h4>
      </footer>
    </div>
    
  )
}

export default App;
